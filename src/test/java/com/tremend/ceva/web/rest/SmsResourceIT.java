package com.tremend.ceva.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tremend.ceva.IntegrationTest;
import com.tremend.ceva.domain.Sms;
import com.tremend.ceva.domain.User;
import com.tremend.ceva.domain.enumeration.SmsStatus;
import com.tremend.ceva.repository.SmsRepository;
import com.tremend.ceva.service.criteria.SmsCriteria;
import com.tremend.ceva.service.dto.SmsDTO;
import com.tremend.ceva.service.mapper.SmsMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SmsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SmsResourceIT {

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_BODY = "AAAAAAAAAA";
    private static final String UPDATED_BODY = "BBBBBBBBBB";

    private static final SmsStatus DEFAULT_STATUS = SmsStatus.SUCCESS;
    private static final SmsStatus UPDATED_STATUS = SmsStatus.FAILED;

    private static final String ENTITY_API_URL = "/api/sms";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SmsRepository smsRepository;

    @Autowired
    private SmsMapper smsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSmsMockMvc;

    private Sms sms;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sms createEntity(EntityManager em) {
        Sms sms = new Sms().phoneNumber(DEFAULT_PHONE_NUMBER).body(DEFAULT_BODY).status(DEFAULT_STATUS);
        return sms;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sms createUpdatedEntity(EntityManager em) {
        Sms sms = new Sms().phoneNumber(UPDATED_PHONE_NUMBER).body(UPDATED_BODY).status(UPDATED_STATUS);
        return sms;
    }

    @BeforeEach
    public void initTest() {
        sms = createEntity(em);
    }

    @Test
    @Transactional
    void createSms() throws Exception {
        int databaseSizeBeforeCreate = smsRepository.findAll().size();
        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);
        restSmsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(smsDTO)))
            .andExpect(status().isCreated());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeCreate + 1);
        Sms testSms = smsList.get(smsList.size() - 1);
        assertThat(testSms.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testSms.getBody()).isEqualTo(DEFAULT_BODY);
        assertThat(testSms.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createSmsWithExistingId() throws Exception {
        // Create the Sms with an existing ID
        sms.setId(1L);
        SmsDTO smsDTO = smsMapper.toDto(sms);

        int databaseSizeBeforeCreate = smsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSmsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(smsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSms() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList
        restSmsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sms.getId().intValue())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].body").value(hasItem(DEFAULT_BODY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getSms() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get the sms
        restSmsMockMvc
            .perform(get(ENTITY_API_URL_ID, sms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sms.getId().intValue()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.body").value(DEFAULT_BODY))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getSmsByIdFiltering() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        Long id = sms.getId();

        defaultSmsShouldBeFound("id.equals=" + id);
        defaultSmsShouldNotBeFound("id.notEquals=" + id);

        defaultSmsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSmsShouldNotBeFound("id.greaterThan=" + id);

        defaultSmsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSmsShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSmsByPhoneNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where phoneNumber equals to DEFAULT_PHONE_NUMBER
        defaultSmsShouldBeFound("phoneNumber.equals=" + DEFAULT_PHONE_NUMBER);

        // Get all the smsList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultSmsShouldNotBeFound("phoneNumber.equals=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void getAllSmsByPhoneNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where phoneNumber not equals to DEFAULT_PHONE_NUMBER
        defaultSmsShouldNotBeFound("phoneNumber.notEquals=" + DEFAULT_PHONE_NUMBER);

        // Get all the smsList where phoneNumber not equals to UPDATED_PHONE_NUMBER
        defaultSmsShouldBeFound("phoneNumber.notEquals=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void getAllSmsByPhoneNumberIsInShouldWork() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where phoneNumber in DEFAULT_PHONE_NUMBER or UPDATED_PHONE_NUMBER
        defaultSmsShouldBeFound("phoneNumber.in=" + DEFAULT_PHONE_NUMBER + "," + UPDATED_PHONE_NUMBER);

        // Get all the smsList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultSmsShouldNotBeFound("phoneNumber.in=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void getAllSmsByPhoneNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where phoneNumber is not null
        defaultSmsShouldBeFound("phoneNumber.specified=true");

        // Get all the smsList where phoneNumber is null
        defaultSmsShouldNotBeFound("phoneNumber.specified=false");
    }

    @Test
    @Transactional
    void getAllSmsByPhoneNumberContainsSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where phoneNumber contains DEFAULT_PHONE_NUMBER
        defaultSmsShouldBeFound("phoneNumber.contains=" + DEFAULT_PHONE_NUMBER);

        // Get all the smsList where phoneNumber contains UPDATED_PHONE_NUMBER
        defaultSmsShouldNotBeFound("phoneNumber.contains=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void getAllSmsByPhoneNumberNotContainsSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where phoneNumber does not contain DEFAULT_PHONE_NUMBER
        defaultSmsShouldNotBeFound("phoneNumber.doesNotContain=" + DEFAULT_PHONE_NUMBER);

        // Get all the smsList where phoneNumber does not contain UPDATED_PHONE_NUMBER
        defaultSmsShouldBeFound("phoneNumber.doesNotContain=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void getAllSmsByBodyIsEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where body equals to DEFAULT_BODY
        defaultSmsShouldBeFound("body.equals=" + DEFAULT_BODY);

        // Get all the smsList where body equals to UPDATED_BODY
        defaultSmsShouldNotBeFound("body.equals=" + UPDATED_BODY);
    }

    @Test
    @Transactional
    void getAllSmsByBodyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where body not equals to DEFAULT_BODY
        defaultSmsShouldNotBeFound("body.notEquals=" + DEFAULT_BODY);

        // Get all the smsList where body not equals to UPDATED_BODY
        defaultSmsShouldBeFound("body.notEquals=" + UPDATED_BODY);
    }

    @Test
    @Transactional
    void getAllSmsByBodyIsInShouldWork() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where body in DEFAULT_BODY or UPDATED_BODY
        defaultSmsShouldBeFound("body.in=" + DEFAULT_BODY + "," + UPDATED_BODY);

        // Get all the smsList where body equals to UPDATED_BODY
        defaultSmsShouldNotBeFound("body.in=" + UPDATED_BODY);
    }

    @Test
    @Transactional
    void getAllSmsByBodyIsNullOrNotNull() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where body is not null
        defaultSmsShouldBeFound("body.specified=true");

        // Get all the smsList where body is null
        defaultSmsShouldNotBeFound("body.specified=false");
    }

    @Test
    @Transactional
    void getAllSmsByBodyContainsSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where body contains DEFAULT_BODY
        defaultSmsShouldBeFound("body.contains=" + DEFAULT_BODY);

        // Get all the smsList where body contains UPDATED_BODY
        defaultSmsShouldNotBeFound("body.contains=" + UPDATED_BODY);
    }

    @Test
    @Transactional
    void getAllSmsByBodyNotContainsSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where body does not contain DEFAULT_BODY
        defaultSmsShouldNotBeFound("body.doesNotContain=" + DEFAULT_BODY);

        // Get all the smsList where body does not contain UPDATED_BODY
        defaultSmsShouldBeFound("body.doesNotContain=" + UPDATED_BODY);
    }

    @Test
    @Transactional
    void getAllSmsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where status equals to DEFAULT_STATUS
        defaultSmsShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the smsList where status equals to UPDATED_STATUS
        defaultSmsShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllSmsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where status not equals to DEFAULT_STATUS
        defaultSmsShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the smsList where status not equals to UPDATED_STATUS
        defaultSmsShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllSmsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultSmsShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the smsList where status equals to UPDATED_STATUS
        defaultSmsShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllSmsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        // Get all the smsList where status is not null
        defaultSmsShouldBeFound("status.specified=true");

        // Get all the smsList where status is null
        defaultSmsShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllSmsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        sms.setUser(user);
        smsRepository.saveAndFlush(sms);
        Long userId = user.getId();

        // Get all the smsList where user equals to userId
        defaultSmsShouldBeFound("userId.equals=" + userId);

        // Get all the smsList where user equals to (userId + 1)
        defaultSmsShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSmsShouldBeFound(String filter) throws Exception {
        restSmsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sms.getId().intValue())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].body").value(hasItem(DEFAULT_BODY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));

        // Check, that the count call also returns 1
        restSmsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSmsShouldNotBeFound(String filter) throws Exception {
        restSmsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSmsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSms() throws Exception {
        // Get the sms
        restSmsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewSms() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        int databaseSizeBeforeUpdate = smsRepository.findAll().size();

        // Update the sms
        Sms updatedSms = smsRepository.findById(sms.getId()).get();
        // Disconnect from session so that the updates on updatedSms are not directly saved in db
        em.detach(updatedSms);
        updatedSms.phoneNumber(UPDATED_PHONE_NUMBER).body(UPDATED_BODY).status(UPDATED_STATUS);
        SmsDTO smsDTO = smsMapper.toDto(updatedSms);

        restSmsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, smsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(smsDTO))
            )
            .andExpect(status().isOk());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
        Sms testSms = smsList.get(smsList.size() - 1);
        assertThat(testSms.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testSms.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testSms.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingSms() throws Exception {
        int databaseSizeBeforeUpdate = smsRepository.findAll().size();
        sms.setId(count.incrementAndGet());

        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSmsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, smsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(smsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSms() throws Exception {
        int databaseSizeBeforeUpdate = smsRepository.findAll().size();
        sms.setId(count.incrementAndGet());

        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSmsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(smsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSms() throws Exception {
        int databaseSizeBeforeUpdate = smsRepository.findAll().size();
        sms.setId(count.incrementAndGet());

        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSmsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(smsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSmsWithPatch() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        int databaseSizeBeforeUpdate = smsRepository.findAll().size();

        // Update the sms using partial update
        Sms partialUpdatedSms = new Sms();
        partialUpdatedSms.setId(sms.getId());

        partialUpdatedSms.phoneNumber(UPDATED_PHONE_NUMBER).body(UPDATED_BODY).status(UPDATED_STATUS);

        restSmsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSms.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSms))
            )
            .andExpect(status().isOk());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
        Sms testSms = smsList.get(smsList.size() - 1);
        assertThat(testSms.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testSms.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testSms.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateSmsWithPatch() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        int databaseSizeBeforeUpdate = smsRepository.findAll().size();

        // Update the sms using partial update
        Sms partialUpdatedSms = new Sms();
        partialUpdatedSms.setId(sms.getId());

        partialUpdatedSms.phoneNumber(UPDATED_PHONE_NUMBER).body(UPDATED_BODY).status(UPDATED_STATUS);

        restSmsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSms.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSms))
            )
            .andExpect(status().isOk());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
        Sms testSms = smsList.get(smsList.size() - 1);
        assertThat(testSms.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testSms.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testSms.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingSms() throws Exception {
        int databaseSizeBeforeUpdate = smsRepository.findAll().size();
        sms.setId(count.incrementAndGet());

        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSmsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, smsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(smsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSms() throws Exception {
        int databaseSizeBeforeUpdate = smsRepository.findAll().size();
        sms.setId(count.incrementAndGet());

        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSmsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(smsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSms() throws Exception {
        int databaseSizeBeforeUpdate = smsRepository.findAll().size();
        sms.setId(count.incrementAndGet());

        // Create the Sms
        SmsDTO smsDTO = smsMapper.toDto(sms);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSmsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(smsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sms in the database
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSms() throws Exception {
        // Initialize the database
        smsRepository.saveAndFlush(sms);

        int databaseSizeBeforeDelete = smsRepository.findAll().size();

        // Delete the sms
        restSmsMockMvc.perform(delete(ENTITY_API_URL_ID, sms.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sms> smsList = smsRepository.findAll();
        assertThat(smsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
