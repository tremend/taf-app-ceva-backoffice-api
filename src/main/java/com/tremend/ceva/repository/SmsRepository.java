package com.tremend.ceva.repository;

import com.tremend.ceva.domain.Sms;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Sms entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SmsRepository extends JpaRepository<Sms, Long>, JpaSpecificationExecutor<Sms> {
    @Query("select sms from Sms sms where sms.user.login = ?#{principal.username}")
    List<Sms> findByUserIsCurrentUser();
}
