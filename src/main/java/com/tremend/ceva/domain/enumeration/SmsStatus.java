package com.tremend.ceva.domain.enumeration;

/**
 * The SmsStatus enumeration.
 */
public enum SmsStatus {
    SUCCESS,
    FAILED,
    PENDING,
}
