package com.tremend.ceva.web.rest.errors;

public class SmsSenderException extends Exception {

    public SmsSenderException(String message) {
        super("Sms service couldn't send sms!", new Throwable(message));
    }
}
