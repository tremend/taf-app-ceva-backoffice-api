package com.tremend.ceva.web.rest;

import com.tremend.ceva.domain.Sms;
import com.tremend.ceva.domain.enumeration.SmsStatus;
import com.tremend.ceva.repository.SmsRepository;
import com.tremend.ceva.service.SmsService;
import com.tremend.ceva.service.sms.SmsSenderService;
import com.tremend.ceva.web.rest.errors.BadRequestAlertException;
import com.tremend.ceva.web.rest.errors.SmsSenderException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.tremend.ceva.domain.Sms}.
 */
@RestController
@RequestMapping("/api")
public class SmsResource {

    private final Logger log = LoggerFactory.getLogger(SmsResource.class);

    private static final String ENTITY_NAME = "sms";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SmsService smsService;

    private final SmsRepository smsRepository;

    private final SmsSenderService smsSenderService;

    public SmsResource(SmsService smsService, SmsRepository smsRepository, SmsSenderService smsSenderService) {
        this.smsService = smsService;
        this.smsRepository = smsRepository;
        this.smsSenderService = smsSenderService;
    }

    /**
     * {@code POST  /sms} : Create a new sms.
     *
     * @param sms the sms to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sms, or with status {@code 400 (Bad Request)} if the sms has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sms")
    public ResponseEntity<Sms> createSms(@RequestBody Sms sms) throws URISyntaxException {
        log.debug("REST request to save Sms : {}", sms);
        if (sms.getId() != null) {
            throw new BadRequestAlertException("A new sms cannot already have an ID", ENTITY_NAME, "idexists");
        }

        try {
            Sms result = smsSenderService.sendSms(sms);

            return ResponseEntity
                .created(new URI("/api/sms/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
        } catch (SmsSenderException exception) {
            return ResponseEntity
                .badRequest()
                .headers(HeaderUtil.createFailureAlert(applicationName, false, ENTITY_NAME, null, exception.getMessage()))
                .body(sms);
        }
    }

    /**
     * {@code PUT  /sms/:id} : Updates an existing sms.
     *
     * @param id the id of the sms to save.
     * @param sms the sms to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sms,
     * or with status {@code 400 (Bad Request)} if the sms is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sms couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sms/{id}")
    public ResponseEntity<Sms> updateSms(@PathVariable(value = "id", required = false) final Long id, @RequestBody Sms sms)
        throws URISyntaxException {
        log.debug("REST request to update Sms : {}, {}", id, sms);
        if (sms.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sms.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!smsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Sms result = smsService.save(sms);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sms.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sms/:id} : Partial updates given fields of an existing sms, field will ignore if it is null
     *
     * @param id the id of the sms to save.
     * @param sms the sms to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sms,
     * or with status {@code 400 (Bad Request)} if the sms is not valid,
     * or with status {@code 404 (Not Found)} if the sms is not found,
     * or with status {@code 500 (Internal Server Error)} if the sms couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/sms/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Sms> partialUpdateSms(@PathVariable(value = "id", required = false) final Long id, @RequestBody Sms sms)
        throws URISyntaxException {
        log.debug("REST request to partial update Sms partially : {}, {}", id, sms);
        if (sms.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sms.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!smsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Sms> result = smsService.partialUpdate(sms);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sms.getId().toString())
        );
    }

    /**
     * {@code GET  /sms} : get all the sms.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sms in body.
     */
    @GetMapping("/sms")
    public ResponseEntity<List<Sms>> getAllSms(Pageable pageable) {
        log.debug("REST request to get a page of Sms");
        Page<Sms> page = smsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sms/:id} : get the "id" sms.
     *
     * @param id the id of the sms to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sms, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sms/{id}")
    public ResponseEntity<Sms> getSms(@PathVariable Long id) {
        log.debug("REST request to get Sms : {}", id);
        Optional<Sms> sms = smsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sms);
    }

    /**
     * {@code DELETE  /sms/:id} : delete the "id" sms.
     *
     * @param id the id of the sms to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sms/{id}")
    public ResponseEntity<Void> deleteSms(@PathVariable Long id) {
        log.debug("REST request to delete Sms : {}", id);
        smsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
