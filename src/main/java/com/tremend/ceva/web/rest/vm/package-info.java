/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tremend.ceva.web.rest.vm;
