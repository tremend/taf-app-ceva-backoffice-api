package com.tremend.ceva.service.sms;

import com.tremend.ceva.config.SmsConfigPropertiesTwilio;
import com.tremend.ceva.web.rest.errors.SmsSenderException;
import com.twilio.Twilio;
import com.twilio.exception.TwilioException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SmsSenderTwilio implements SmsSender {

    private final SmsConfigPropertiesTwilio smsConfigPropertiesTwilio;

    @PostConstruct
    private void init() {
        Twilio.init(smsConfigPropertiesTwilio.getAccountSid(), smsConfigPropertiesTwilio.getAuthToken());
    }

    @Override
    public void sendMessage(String to, String body) throws SmsSenderException {
        log.debug("Trying to send message with Twilio with body {} to {} ...", body, to);
        try {
            Message.creator(new PhoneNumber(to), new PhoneNumber(smsConfigPropertiesTwilio.getFrom()), body).create();

            log.debug("Sending message with body {} to {} successfully", body, to);
        } catch (TwilioException e) {
            log.debug("Sending message with body {} to {} failed", body, to);

            throw new SmsSenderException(e.getMessage().toLowerCase());
        }
    }
}
