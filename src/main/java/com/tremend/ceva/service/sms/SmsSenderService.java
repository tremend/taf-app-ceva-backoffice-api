package com.tremend.ceva.service.sms;

import com.tremend.ceva.domain.Sms;
import com.tremend.ceva.domain.User;
import com.tremend.ceva.domain.enumeration.SmsStatus;
import com.tremend.ceva.service.SmsService;
import com.tremend.ceva.service.UserService;
import com.tremend.ceva.web.rest.errors.BadRequestAlertException;
import com.tremend.ceva.web.rest.errors.SmsSenderException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SmsSenderService {

    private final SmsService smsService;

    private final UserService userService;

    private final SmsSender smsSender;

    public Sms sendSms(Sms sms) throws SmsSenderException {
        log.debug("Request to save Sms : {}", sms);

        User user = userService
            .getUserWithAuthorities()
            .orElseThrow(() -> new BadRequestAlertException("Unauthorized user!", "User", "idnull"));

        setSmsDetails(sms, user);
        setStatusAndSaveSms(sms, SmsStatus.PENDING);

        try {
            smsSender.sendMessage(sms.getPhoneNumber(), sms.getBody());
            log.debug("Successfully sent Sms...");

            setStatusAndSaveSms(sms, SmsStatus.SUCCESS);

            return sms;
        } catch (SmsSenderException exception) {
            log.debug(exception.getMessage());

            setStatusAndSaveSms(sms, SmsStatus.FAILED);

            throw exception;
        }
    }

    private void setStatusAndSaveSms(Sms sms, SmsStatus status) {
        sms.setStatus(status);
        smsService.save(sms);
    }

    private void setSmsDetails(Sms sms, User user) {
        sms.setPhoneNumber("+" + sms.getPhoneNumber());
        sms.setUser(user);
    }
}
