package com.tremend.ceva.service.sms;

import com.tremend.ceva.web.rest.errors.SmsSenderException;

public interface SmsSender {
    void sendMessage(String to, String body) throws SmsSenderException;
}
