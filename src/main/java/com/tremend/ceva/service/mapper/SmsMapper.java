package com.tremend.ceva.service.mapper;

import com.tremend.ceva.domain.*;
import com.tremend.ceva.service.dto.SmsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sms} and its DTO {@link SmsDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface SmsMapper extends EntityMapper<SmsDTO, Sms> {
    @Mapping(target = "user", source = "user", qualifiedByName = "id")
    SmsDTO toDto(Sms s);
}
