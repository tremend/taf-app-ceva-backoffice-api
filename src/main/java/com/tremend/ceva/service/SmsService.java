package com.tremend.ceva.service;

import com.tremend.ceva.domain.Sms;
import com.tremend.ceva.repository.SmsRepository;
import com.tremend.ceva.service.dto.SmsDTO;
import com.tremend.ceva.service.mapper.SmsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Sms}.
 */
@Service
@Transactional
public class SmsService {

    private final Logger log = LoggerFactory.getLogger(SmsService.class);

    private final SmsRepository smsRepository;

    private final SmsMapper smsMapper;

    public SmsService(SmsRepository smsRepository, SmsMapper smsMapper) {
        this.smsRepository = smsRepository;
        this.smsMapper = smsMapper;
    }

    /**
     * Save a sms.
     *
     * @param smsDTO the entity to save.
     * @return the persisted entity.
     */
    public SmsDTO save(SmsDTO smsDTO) {
        log.debug("Request to save Sms : {}", smsDTO);
        Sms sms = smsMapper.toEntity(smsDTO);
        sms = smsRepository.save(sms);
        return smsMapper.toDto(sms);
    }

    /**
     * Partially update a sms.
     *
     * @param smsDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SmsDTO> partialUpdate(SmsDTO smsDTO) {
        log.debug("Request to partially update Sms : {}", smsDTO);

        return smsRepository
            .findById(smsDTO.getId())
            .map(
                existingSms -> {
                    smsMapper.partialUpdate(existingSms, smsDTO);

                    return existingSms;
                }
            )
            .map(smsRepository::save)
            .map(smsMapper::toDto);
    }

    /**
     * Get all the sms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SmsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Sms");
        return smsRepository.findAll(pageable).map(smsMapper::toDto);
    }

    /**
     * Get one sms by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SmsDTO> findOne(Long id) {
        log.debug("Request to get Sms : {}", id);
        return smsRepository.findById(id).map(smsMapper::toDto);
    }

    /**
     * Delete the sms by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sms : {}", id);
        smsRepository.deleteById(id);
    }
}
