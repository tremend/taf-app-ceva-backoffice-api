package com.tremend.ceva.service.dto;

import com.tremend.ceva.domain.enumeration.SmsStatus;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.tremend.ceva.domain.Sms} entity.
 */
public class SmsDTO implements Serializable {

    private Long id;

    private String phoneNumber;

    private String body;

    private SmsStatus status;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public SmsStatus getStatus() {
        return status;
    }

    public void setStatus(SmsStatus status) {
        this.status = status;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SmsDTO)) {
            return false;
        }

        SmsDTO smsDTO = (SmsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, smsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SmsDTO{" +
            "id=" + getId() +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", body='" + getBody() + "'" +
            ", status='" + getStatus() + "'" +
            ", user=" + getUser() +
            "}";
    }
}
